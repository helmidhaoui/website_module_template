# -*- coding: utf-8 -*-
{
    'name': "Website module template",

    'summary': "",

    'description': """
        
    """,

    'author': "Helmi Dhaoui - Tunisofts",
    'website': "http://www.tunisofts.com",

    'category': 'website',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','website'],

    # always loaded
    'data': [
        'views/homepage.xml',
        'views/new_page.xml',
        'views/assets.xml',
    ],
}